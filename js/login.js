// Import the functions you need from the SDKs you need
import { getAuth, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js";
// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyARRVgwi0-12vg46WN1Z6n6QyMgPwSHZgE",
  authDomain: "proyectofinal-72288.firebaseapp.com",
  projectId: "proyectofinal-72288",
  storageBucket: "proyectofinal-72288.appspot.com",
  messagingSenderId: "698316000887",
  appId: "1:698316000887:web:bcc84491c463068f860072"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

// Obtén una instancia de autenticación
const auth = getAuth(app);

const formulario = document.getElementById("formulario");
const emailInput = document.getElementById("exampleInputEmail1");
const passwordInput = document.getElementById("exampleInputPassword1");
const errorMensaje = document.getElementById("errorMensaje");

formulario.addEventListener("submit", async (e) => {
  e.preventDefault();

  const email = emailInput.value;
  const password = passwordInput.value;

  try {
    // Autentica al administrador utilizando Firebase
    await signInWithEmailAndPassword(auth, email, password);
    // Si el inicio de sesión es exitoso, puedes redirigir a la página de administrador o realizar otras acciones
    window.location.href = "/html/Administracion.html";
    console.log("se ingreso correctamente");
  } catch (error) {
    if(email == "" || password == "")
    errorMensaje.textContent = "Ingresa el campo vacio";
  else{
    errorMensaje.textContent = "Error de inicio de sesión. Verifica tu correo y contraseña.";
    alert("Error de inicio de sesión. Verifica tu correo y contraseña.")
    console.error("Error de inicio de sesión:", error);
  }
  }
});
