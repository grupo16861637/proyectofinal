document.addEventListener("DOMContentLoaded", function() {
    var scrollLinks = document.querySelectorAll('.scroll-link');

    scrollLinks.forEach(function(link) {
        link.addEventListener('click', function(event) {
            event.preventDefault();

            var targetId = this.getAttribute('href').substring(1);
            var targetElement = document.getElementById(targetId);
            if (targetElement) {
                var yOffset = targetElement.getBoundingClientRect().top + window.scrollY;
                window.scrollTo({
                    top: yOffset,
                    behavior: 'smooth' // Hace que el desplazamiento sea suave
                });
            }
        });
    });
});