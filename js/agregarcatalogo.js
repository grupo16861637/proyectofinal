import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getFirestore, collection, addDoc, getDocs } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-firestore.js";
import { getStorage, ref, uploadBytes, getDownloadURL } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js";

const firebaseConfig = {
apiKey: "AIzaSyARRVgwi0-12vg46WN1Z6n6QyMgPwSHZgE",
authDomain: "proyectofinal-72288.firebaseapp.com",
databaseURL: "https://proyectofinal-72288-default-rtdb.firebaseio.com",
projectId: "proyectofinal-72288",
storageBucket: "proyectofinal-72288.appspot.com",
messagingSenderId: "698316000887",
appId: "1:698316000887:web:bcc84491c463068f860072"
};

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);
const storage = getStorage(app);

document.addEventListener('DOMContentLoaded', function () {
const dbRef = collection(db, "productos");
const seccionesContainer = document.getElementById('secciones');

getDocs(dbRef)
.then((querySnapshot) => {
  querySnapshot.forEach((doc) => {
    const data = doc.data();
    const nombre = data.nombre;
    const descripcion = data.descripcion;
    const precio = data.precio;
    const imageUrl = data.url;

    const article = document.createElement('article');
    article.className = 'caja1';

    const productImage = document.createElement('img');
    productImage.src = imageUrl;
    productImage.alt = '';
    article.appendChild(productImage);

   const productTitle = document.createElement('h2');
    productTitle.textContent = nombre;
    article.appendChild(productTitle);

    const productDescription = document.createElement('p');
    productDescription.textContent = descripcion;
    article.appendChild(productDescription);

    const productPrice = document.createElement('p');
    productPrice.innerHTML = `<strong>Precio: $${precio}</strong>`;
    article.appendChild(productPrice);

    
    seccionesContainer.appendChild(article);
  });
})
.catch((error) => {
  console.error("Error al cargar productos: ", error);
});
});
