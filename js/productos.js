import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getFirestore, collection, addDoc, getDocs , deleteDoc, doc, setDoc, getDoc} from "https://www.gstatic.com/firebasejs/10.5.2/firebase-firestore.js";
import { getStorage, ref, uploadBytes, getDownloadURL } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js";

const firebaseConfig = {
    apiKey: "AIzaSyARRVgwi0-12vg46WN1Z6n6QyMgPwSHZgE",
    authDomain: "proyectofinal-72288.firebaseapp.com",
    databaseURL: "https://proyectofinal-72288-default-rtdb.firebaseio.com",
    projectId: "proyectofinal-72288",
    storageBucket: "proyectofinal-72288.appspot.com",
    messagingSenderId: "698316000887",
    appId: "1:698316000887:web:bcc84491c463068f860072"
  };

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);
const storage = getStorage(app);

const btnagregar = document.getElementById('btnagregar');

btnagregar.addEventListener('click', async function() {
event.preventDefault();

// Obtén los valores de los campos del formulario
const txtCodigo = document.getElementById('txtCodigo').value;
const txtNombre = document.getElementById('txtNombre').value;
const txtdescripcion = document.getElementById('txtdescripcion').value;
const txtPrecio = document.getElementById('txtPrecio').value;
const imageInput = document.getElementById('imageInput');
const selectedFile = imageInput.files[0];

if (!txtCodigo || !txtNombre || !txtdescripcion || !txtPrecio || !selectedFile) {
    alert('Por favor, complete todos los campos y seleccione una imagen.');
    return;
}

// Realiza una consulta para verificar si un documento con el mismo código ya existe
const productoRef = doc(db, "productos", txtCodigo);
const productoSnap = await getDoc(productoRef);

if (productoSnap.exists()) {
    alert("El código ya existe en la base de datos.");
    document.getElementById('mensaje').textContent = "El codigo ya existe";
} else {
    // Sube la imagen a Firebase Storage
    const storageRef = ref(storage, 'images/' + selectedFile.name);
    await uploadBytes(storageRef, selectedFile);

    // Obtiene la URL de la imagen subida
    const imageUrl = await getDownloadURL(storageRef);

    // Crea un objeto de datos con el código, nombre, descripción, precio y URL de la imagen
    const productData = {
        codigo: txtCodigo,  // Utiliza el código ingresado manualmente
        nombre: txtNombre,
        descripcion: txtdescripcion,
        precio: txtPrecio,
        url: imageUrl
    };

    // Agrega los datos a Firestore con el código ingresado
    await setDoc(doc(db, "productos", txtCodigo), productData);
    document.getElementById('mensaje').textContent = "Producto agregado con éxito";
    console.log("Producto agregado con código: ", txtCodigo);
}

// Limpia el formulario
document.getElementById('txtCodigo').value = '';
document.getElementById('txtNombre').value = '';
document.getElementById('txtdescripcion').value = '';
document.getElementById('txtPrecio').value = '';
document.getElementById('imageInput').value = '';
document.getElementById('txtUrl').value = '';


// Carga nuevamente los datos
cargarDatos();
});



const tablaProductosBody = document.querySelector('#tablaProductos tbody');

async function cargarDatos() {
    const querySnapshot = await getDocs(collection(db, "productos"));
    tablaProductosBody.innerHTML = '';

    querySnapshot.forEach((doc) => {
        const data = doc.data();
        const codigo = doc.id;
        const nombre = data.nombre;
        const descripcion = data.descripcion;
        const precio = data.precio;
        const imageUrl = data.url;

        const newRow = tablaProductosBody.insertRow(tablaProductosBody.rows.length);
        const codigoCell = newRow.insertCell(0);
        const nombreCell = newRow.insertCell(1);
        const descripcionCell = newRow.insertCell(2);
        const precioCell = newRow.insertCell(3);
        const imagenCell = newRow.insertCell(4);

        codigoCell.textContent = codigo;
        nombreCell.textContent = nombre;
        descripcionCell.textContent = descripcion;
        precioCell.textContent = "$"+precio;

        const img = document.createElement("img");
        img.src = imageUrl;
        img.width = 100;
        imagenCell.appendChild(img);
    });
}

const btnBorrar = document.getElementById('btnBorrar');
btnBorrar.addEventListener('click', async function() {
event.preventDefault();

// Obtiene el código del producto a borrar desde el nuevo input
const codigoABorrar = document.getElementById('txtCodigo').value;

if (codigoABorrar) {
    try {
        // Elimina el documento en Firestore usando el código proporcionado
        await deleteDoc(doc(db, "productos", codigoABorrar));
        console.log(`Producto con código ${codigoABorrar} eliminado.`);
        cargarDatos(); // Vuelve a cargar los datos después de eliminar el producto.
    } catch (error) {
        console.error('Error al eliminar el producto:', error);
    }
}
    else{
        alert("Ingresa el codigo del producto")
    }
});

// Agrega un evento de clic al botón "Buscar"
const btnBuscar = document.getElementById('btnBuscar');
btnBuscar.addEventListener('click', async function() {
event.preventDefault();

// Obtiene el código del producto a buscar desde el nuevo input
const codigoABuscar = document.getElementById('txtCodigo').value;

if (codigoABuscar) {
    // Realiza una consulta a la base de datos para buscar el producto por código
    const productoRef = doc(db, "productos", codigoABuscar);
    const productoSnap = await getDoc(productoRef);

    if (productoSnap.exists()) {
        const data = productoSnap.data();
        const nombre = data.nombre;
        const descripcion = data.descripcion;
        const precio = data.precio;
        const imageUrl = data.url;

        // Limpia la tabla de productos
        tablaProductosBody.innerHTML = '';

        // Agrega el producto encontrado a la tabla
        const newRow = tablaProductosBody.insertRow(tablaProductosBody.rows.length);
        const codigoCell = newRow.insertCell(0);
        const nombreCell = newRow.insertCell(1);
        const descripcionCell = newRow.insertCell(2);
        const precioCell = newRow.insertCell(3);
        const imagenCell = newRow.insertCell(4);

        codigoCell.textContent = codigoABuscar;
        nombreCell.textContent = nombre;
        descripcionCell.textContent = descripcion;
        precioCell.textContent = precio;

        const img = document.createElement("img");
        img.src = imageUrl;
        img.width = 100;
        imagenCell.appendChild(img);
    } else {
        alert('Producto no encontrado.');
    }
} else {
    alert('Ingresa un código de producto válido para buscar.');
}
});

// Agrega un evento de clic al botón "Actualizar" para que funcione
btnActualizar.addEventListener('click', async function() {
event.preventDefault();

const codigoAActualizar = document.getElementById('txtCodigo').value;
const txtNombre = document.getElementById('txtNombre').value;
const txtdescripcion = document.getElementById('txtdescripcion').value;
const txtPrecio = document.getElementById('txtPrecio').value;

if (codigoAActualizar && txtNombre && txtdescripcion && txtPrecio) {
    const productoRef = doc(db, "productos", codigoAActualizar);
    
    // Actualiza los detalles del producto en la base de datos
    await setDoc(productoRef, {
        nombre: txtNombre,
        descripcion: txtdescripcion,
        precio: txtPrecio
    }, { merge: true });

    // Actualiza la imagen si se seleccionó una nueva
    const imageInputActualizar = document.getElementById('imageInput');
    const selectedFile = imageInputActualizar.files[0];
    
    if (selectedFile) {
        const storageRef = ref(storage, 'images/' + selectedFile.name);
        await uploadBytes(storageRef, selectedFile);

        const imageUrl = await getDownloadURL(storageRef);
        
        // Actualiza la URL de la imagen en Firestore
        await setDoc(productoRef, { url: imageUrl }, { merge: true });
    }

    // Muestra un mensaje de éxito
    document.getElementById('mensaje').textContent = 'Producto actualizado con éxito.';
} else {
    document.getElementById('mensaje').textContent="Completa todos los campos antes de actualizar";
    alert('Completa todos los campos antes de actualizar.');
}

// Limpia el formulario después de la actualización
document.getElementById('txtCodigo').value = '';
document.getElementById('txtNombre').value = '';
document.getElementById('txtdescripcion').value = '';
document.getElementById('txtPrecio').value = '';
document.getElementById('txtUrl').value = '';
document.getElementById('mensaje').textContent='';

// Recarga los datos en la tabla
location.reload();
cargarDatos();
});

const btnlimpiar = document.getElementById('limpia');
btnlimpiar.addEventListener('click', function(){
document.getElementById('txtCodigo').value = '';
document.getElementById('txtNombre').value = '';
document.getElementById('txtdescripcion').value = '';
document.getElementById('txtPrecio').value = '';
document.getElementById('txtUrl').value = '';
document.getElementById('mensaje').textContent='';
});


cargarDatos();

